package dsmpf.holdem;

import dsmpf.holdem.core.GameContext;
import dsmpf.holdem.core.GameDriver;
import dsmpf.holdem.core.GameEvent;
import dsmpf.holdem.core.Player;
import dsmpf.holdem.network.*;
import dsmpf.holdem.network.HolePunchingRmiService.HolePunchingRmiServiceImpl;
import dsmpf.holdem.network.HolePunchingTcpService.HolePunchingTcpServiceImpl;
import dsmpf.holdem.network.PingRmiService.PingRmiServiceImpl;
import dsmpf.holdem.network.RegistrationRmiService.RegistrationRmiServiceImpl;
import dsmpf.holdem.storage.DistributedKeyValueStorage;
import dsmpf.holdem.storage.LocalKeyValueStorage;
import dsmpf.holdem.storage.StorageTransactionRmiService;
import dsmpf.holdem.storage.StorageTransactionRmiService.StorageTransactionRmiServiceImpl;
import dsmpf.holdem.utils.GameShell;
import dsmpf.holdem.utils.ProgramArgumentParser;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.logging.*;

public class Main {

    public static PingRmiServiceImpl pingRmiService;
    public static RegistrationRmiServiceImpl bootstrapRmiService;
    public static HolePunchingRmiServiceImpl holePunchingRmiService;
    public static HolePunchingTcpServiceImpl holePunchingTcpService;
    public static StorageTransactionRmiServiceImpl storageTransactionRmiService;
    public static Registry rmiRegistry;
    public static RmiServiceDiscovery rmiServiceDiscovery;
    public static Logger mainLogger = Logger.getLogger("main");
    public static LocalKeyValueStorage localKeyValueStorage;
    public static DistributedKeyValueStorage distributedKeyValueStorage;


    public static Config configuration(String[] args) throws IOException {
        ProgramArgumentParser argumentParser = new ProgramArgumentParser();
        Logger rootLogger = LogManager.getLogManager().getLogger("");
        rootLogger.setLevel(Level.FINE);
        for (Handler h : rootLogger.getHandlers()) {
            h.setFormatter(new Formatter() {
                @Override
                public String format(LogRecord logRecord) {
                    return String.format("[%10s][%20s]: %s\n",
                            logRecord.getLevel().toString(),
                            logRecord.getLoggerName(),
                            logRecord.getMessage());
                }
            });
            h.setLevel(Level.INFO);
        }
        Config config = argumentParser.parse(args, new Config());
        if (!config.disableShutdownHook) {
            mainLogger.info("enabled - press Ctrl-C to finish the program");
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                public void run() {
                    mainLogger.info("Interrupt signal received");
                    System.exit(0);
                }
            }));
        }
        return config;
    }

    public static void main(String[] args) throws IOException, AlreadyBoundException, NotBoundException, InterruptedException {
        System.setProperty("sun.rmi.transport.tcp.logLevel", "INFO");
        System.setProperty("sun.rmi.loader.logLevel", "INFO");
        System.setProperty("sun.rmi.client.ref", "INFO");
        Config config = configuration(args);
        System.out.println(config.toString());

        if (config.advertisedAddress == null &&
                config.bootstrapServerAddr == null) {
            mainLogger.severe("Unable to either connect to existing game network, or to begin a new one. Exiting");
            System.exit(1);
        }
        if (config.advertisedAddress != null) {
            System.setProperty("java.rmi.server.hostname", config.advertisedAddress.split(":")[0]);
            var advertisedPort = Integer.parseInt(config.advertisedAddress.split(":")[1]);
            mainLogger.info("Setting up RMI Registry using advertised port = " + advertisedPort);
            rmiRegistry = LocateRegistry.createRegistry(advertisedPort);
        } else {
            mainLogger.info("Setting up RMI Registry using port 1099");
            rmiRegistry = LocateRegistry.createRegistry(1099);
        }
        var logManager = LogManager.getLogManager();
        rmiServiceDiscovery = new RmiServiceDiscovery(rmiRegistry);

        // local services initialization
        if (config.advertisedAddress != null) {
            mainLogger.info("This node can be used as bootstrap service and hole punching service due to available public address");
            bootstrapRmiService = RegistrationRmiServiceImpl.register(rmiServiceDiscovery, RegistrationRmiService.NAME);
            holePunchingRmiService = HolePunchingRmiServiceImpl.register(rmiServiceDiscovery, HolePunchingRmiService.NAME, config.holePunchingServerTcpPort);
        } else {
            mainLogger.info("This node won't be used as bootstrap service and hole punching service - no public address");
        }
        pingRmiService = PingRmiServiceImpl.register(rmiServiceDiscovery, PingRmiService.NAME);
        localKeyValueStorage = new LocalKeyValueStorage();
        distributedKeyValueStorage = new DistributedKeyValueStorage(localKeyValueStorage, rmiServiceDiscovery);
        storageTransactionRmiService = StorageTransactionRmiServiceImpl.register(rmiServiceDiscovery,
                StorageTransactionRmiService.NAME, localKeyValueStorage);
        // if is able to connect to bootstrap server, then synchronize current game state
        // if is not, setup itself as leader

        // after initialization of all local services, register node in node identity manager
        var advertisedAddrArr = config.advertisedAddress.split(":");
        var advertisedPort = Integer.parseInt(advertisedAddrArr[1]);
        if (config.bootstrapServerAddr != null) {
            // register current node in network using existing bootstrap rmi service with public ip address
            // this is done without rmi service discovery because we don't know uuids yet
            var bootstrapAddrArr = config.bootstrapServerAddr.split(":");
            var bootstrapInetAddr = new InetSocketAddress(bootstrapAddrArr[0], Integer.parseInt(bootstrapAddrArr[1]));
            RegistrationRmiService bootstrapRmiServiceClient = rmiServiceDiscovery.lookup(bootstrapInetAddr, RegistrationRmiService.NAME);
            NodeIdentityManager.registerSelf(bootstrapRmiServiceClient, advertisedAddrArr[0], advertisedPort, config.isBehindNat);
            //var holePunchingInetAddr = new InetSocketAddress(bootstrapAddrArr[0],0);
            HolePunchingRmiService holePunchingRmiServiceClient = rmiServiceDiscovery.lookup(bootstrapInetAddr, HolePunchingRmiService.NAME);
            var tcpHolePuncherPort = holePunchingRmiServiceClient.getHolePuncherPort();
            rmiServiceDiscovery.setTcpHolePuncherEndpoint(new InetSocketAddress(bootstrapAddrArr[0], tcpHolePuncherPort));
        } else {
            // register the first node in network using current node's advertised public address
            NodeIdentityManager.registerSelf(advertisedAddrArr[0], advertisedPort, config.isBehindNat);
        }

        // now we are able to initialize distributed key-value storage and its server-service
        distributedKeyValueStorage.prepare();

        var selfIdentity = NodeIdentityManager.getSelfIdentity();
        if (selfIdentity.isDealer) {
            mainLogger.info("Current node is registered as a dealer");
            distributedKeyValueStorage.put("game:dealer_id", selfIdentity.id);
            var gameEventServer = new GameEventServer(rmiServiceDiscovery);
            gameEventServer.start();

            new Thread(() -> {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                var result = gameEventServer.publish(selfIdentity, new GameEvent(GameEvent.Command.ACTION, "Press any key to start the game"));
                System.out.println("Result = " + result.toString());

                System.out.println("Game started!");

                ArrayList<Player> playerList = new ArrayList<>();
                for (var nodeId : NodeIdentityManager.getIdentityList()) {
                    playerList.add(new Player(nodeId.id));
                }
                System.out.println("Number of players: " + playerList.size());
                var gameContext = new GameContext(playerList);

                GameDriver gameDriver = new GameDriver(gameEventServer, gameContext);
                gameDriver.run();

            }).start();
        } else {
            var currentDealerId = distributedKeyValueStorage.get("game:dealer_id");
            System.out.println("Current dealer id: " + currentDealerId);
        }

        System.out.println("Number of nodes: " + NodeIdentityManager.getNodesCount());
        // temporary shell
        //var commandShell = new CommandShell();
        //commandShell.loop();

        // game shell
        var gameShell = new GameShell(rmiServiceDiscovery, distributedKeyValueStorage);
        gameShell.loop();

        if (bootstrapRmiService != null) {
            bootstrapRmiService.unregister(rmiServiceDiscovery);
        }
        holePunchingRmiService.unregister(rmiServiceDiscovery);
        pingRmiService.unregister(rmiServiceDiscovery);
        if (config.disableShutdownHook) {
            System.exit(0);
        }
    }

}
