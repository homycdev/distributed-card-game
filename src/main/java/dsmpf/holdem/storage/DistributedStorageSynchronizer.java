package dsmpf.holdem.storage;

import dsmpf.holdem.network.NodeIdentity;
import dsmpf.holdem.network.NodeIdentityManager;
import dsmpf.holdem.network.RegistrationRmiService;
import dsmpf.holdem.network.RmiServiceDiscovery;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DistributedStorageSynchronizer {

    private Logger logger = Logger.getLogger("StorageSynchronizer");
    private final LocalKeyValueStorage localStorage;
    private final RmiServiceDiscovery rmiServiceDiscovery;
    private Map<UUID, StorageTransactionRmiService> nodeTransactionServices = new HashMap<>();

    /**
     * Simple constructor for not prepared services
     *
     * @param localStorage
     * @param rmiServiceDiscovery
     */
    public DistributedStorageSynchronizer(LocalKeyValueStorage localStorage,
                                          RmiServiceDiscovery rmiServiceDiscovery) {
        this.localStorage = localStorage;
        this.rmiServiceDiscovery = rmiServiceDiscovery;
    }

    private void updateTransactionServiceOfNode(NodeIdentity identity) {
        try {
            nodeTransactionServices.put(identity.id,
                    rmiServiceDiscovery.lookup(identity.id, StorageTransactionRmiService.NAME)
            );
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fetches nodes lists, should be called after registration, finds StorageTransactionRMIService on those nodes
     * And establishes connection
     * Creates callback for node identity manager to create new connection with new nodes that are going to be added
     */
    public void prepare() {
        nodeTransactionServices.clear();
        var knownNodesList = NodeIdentityManager.getIdentityList();
        knownNodesList.forEach(this::updateTransactionServiceOfNode);
        var publicServices = knownNodesList.stream()
                .filter(identity -> !identity.isBehindNat)
                .collect(Collectors.toList());
        if (publicServices.size() > 0) {
            try {
                var existingTransactions = nodeTransactionServices.get(publicServices.get(0).id).fetch();
                for (var transactions : existingTransactions) {
                    localStorage.put(transactions.key, transactions.value);
                }
                logger.info("Fetched " + existingTransactions.size() + " existing transactions from node " + publicServices.get(0).id);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            logger.info("Unable to fetch existing transactions: all " + knownNodesList.size() + " known nodes are behind nat");
        }
        logger.info("Prepared " + nodeTransactionServices.size() + " node transaction services for distributed storage");
        NodeIdentityManager.addOnNodeChangeListenerList(new RegistrationRmiService.OnNodeChangeListener() {
            @Override
            public void onNodeRegister(NodeIdentity newRegisteredNode) throws RemoteException {
                updateTransactionServiceOfNode(newRegisteredNode);
            }

            @Override
            public void onNodeUnregister(NodeIdentity removedNode) throws RemoteException {

            }
        });
    }

    /**
     * Used by distributed key-value-storage to push new data into all known nodes
     *
     * @param transactionId
     * @param key
     * @param value
     * @param <V>
     * @return true if transaction worked out on all nodes
     */
    public <V> boolean write(int transactionId, String key, V value) {
        var entrySet = nodeTransactionServices.entrySet();
        for (var transactionServiceEntry : entrySet) {
            try {
                transactionServiceEntry.getValue().commit(new StorageTransactionRmiService.Transaction(
                        transactionId, key, value
                ));
            } catch (RemoteException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
