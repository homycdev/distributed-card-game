package dsmpf.holdem.storage;

import dsmpf.holdem.network.RmiServiceDiscovery;

public class DistributedKeyValueStorage implements KeyValueStorage {

    private final LocalKeyValueStorage localStorage;
    private final DistributedStorageSynchronizer synchronizer;

    public DistributedKeyValueStorage(LocalKeyValueStorage localStorage,
                                      RmiServiceDiscovery rmiServiceDiscovery) {
        this.localStorage = localStorage;
        this.synchronizer = new DistributedStorageSynchronizer(localStorage, rmiServiceDiscovery);
    }

    public void prepare() {
        synchronizer.prepare();
    }

    @Override
    public int currentStateId() {
        return localStorage.currentStateId();
    }

    @Override
    public <V> V put(String key, V value) {
        V currentValue = localStorage.get(key);
        if ((currentValue != null && !currentValue.equals(value)) ||
                (value != null && currentValue == null)) {
            System.out.println("ds -> put");
            //V localValue = localStorage.put(key, value);
            if (synchronizer.write(currentStateId() + 1, key, value)) {
                return value;
            } else {
                System.out.println("ds -> put -> sync false");
                return currentValue;
            }
        } else {
            System.out.println("ds -> !put");
            return value;
        }
    }

    @Override
    public <V> V get(String key) {
        V localValue = localStorage.get(key);
        return localValue;
    }
}
