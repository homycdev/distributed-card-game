package dsmpf.holdem.storage;

public interface KeyValueStorage {
    /**
     * Simplified custom Map
     *
     * @return id of last transaction
     */
    public int currentStateId();

    /**
     * Put - puts element to the map
     * If null val is put -> will delete the key
     * If new value == old value -> patch the key
     *
     * @param key
     * @param value
     * @param <V>
     * @return what have been put
     */
    public <V> V put(String key, V value);

    /**
     * @param key
     * @param <V>
     * @return returns current value from local storage
     */
    public <V> V get(String key);

}
