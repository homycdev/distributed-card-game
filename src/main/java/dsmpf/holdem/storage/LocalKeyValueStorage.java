package dsmpf.holdem.storage;

import java.util.HashMap;
import java.util.Map;

public class LocalKeyValueStorage implements KeyValueStorage {

    public int lastUpdateId = 0;
    public Map<String, Object> storageMap;

    public LocalKeyValueStorage() {
        storageMap = new HashMap<>();
    }

    public int currentStateId() {
        return lastUpdateId;
    }

    @Override
    public <V> V put(String key, V value) {
        if (key == null) {
            return null;
        }
        lastUpdateId++;
        if (value == null) {
            System.out.println("ls -> put -> remove");
            storageMap.remove(key);
            return null;
        } else {
            System.out.println("ls -> put");
            storageMap.put(key, value);

            return value;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <V> V get(String key) {
        if (key == null) {
            return null;
        }
        if (storageMap.containsKey(key)) {
            return (V) storageMap.get(key);
        } else {
            return null;
        }
    }
}
