package dsmpf.holdem.storage;

import dsmpf.holdem.network.RmiServiceDiscovery;

import java.io.Serializable;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.List;

public interface StorageTransactionRmiService extends Remote {

    public static String NAME = "StorageTransactionService";

    public static class Transaction implements Serializable {
        int id;
        String key;
        Object value;

        public Transaction(int id, String key, Object value) {
            this.id = id;
            this.key = key;
            this.value = value;
        }
    }

    default public List<Transaction> fetch() throws RemoteException {
        return fetch(0);
    }

    /**
     * Used by new nodes to know transactions that had been before node's joining the net.
     *
     * @param last
     * @return
     * @throws RemoteException
     */
    public List<Transaction> fetch(int last) throws RemoteException;

    /**
     * Re-addresses the record to local KeyValueStorage
     * Also can be pushed to distributed KeyValStorage
     *
     * @param transaction
     * @return
     * @throws RemoteException
     */
    public boolean commit(Transaction transaction) throws RemoteException;

    public class StorageTransactionRmiServiceImpl implements StorageTransactionRmiService {

        private final String serviceName;
        private final KeyValueStorage slaveStorage;
        private int lastTransactionId;
        private List<Transaction> appliedTransactions;

        public StorageTransactionRmiServiceImpl(String serviceName, KeyValueStorage slaveStorage) {
            this.appliedTransactions = new LinkedList<>();
            this.serviceName = serviceName;
            this.slaveStorage = slaveStorage;
            this.lastTransactionId = 0;
        }

        @Override
        public List<Transaction> fetch(int last) throws RemoteException {
            if (last == 0) {
                return appliedTransactions;
            } else {
                // TODO: return fixed amount
                return appliedTransactions;
            }
        }

        @Override
        public boolean commit(Transaction transaction) throws RemoteException {
            if (lastTransactionId >= transaction.id) {
                return false;
            }
            slaveStorage.put(transaction.key, transaction.value);
            lastTransactionId = transaction.id;
            appliedTransactions.add(transaction);
            return true;
        }

        public static StorageTransactionRmiServiceImpl register(RmiServiceDiscovery serviceDiscovery, String serviceName, KeyValueStorage slaveStorage) throws AlreadyBoundException, RemoteException {
            var service = new StorageTransactionRmiServiceImpl(serviceName, slaveStorage);
            return serviceDiscovery.register(serviceName, service);
        }

        public void unregister(RmiServiceDiscovery serviceDiscovery) throws RemoteException, NotBoundException {
            serviceDiscovery.unregister(this.serviceName);
        }
    }

}
