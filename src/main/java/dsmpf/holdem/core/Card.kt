package dsmpf.holdem.core

class Card constructor(val rank: Rank, val suit: Suit) {

    enum class Rank {
        RANK_2, RANK_3, RANK_4, RANK_5, RANK_6, RANK_7, RANK_8, RANK_9, RANK_10, JACK, QUEEN, KING, ACE
    }

    enum class Suit {
        DIAMONDS, HEARTS, SPADES, CLUBS
    }
}