package dsmpf.holdem.core;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static java.lang.Integer.max;

public class CardRanker {

    public static int rank(List<Card> cards) {
        if (royalFlush(cards)) {
            return 10;
        } else if (straightFlush(cards))
            return 9;
        else if (fourOfAKind(cards))
            return 8;
        else if (fullHouse(cards))
            return 7;
        else if (flush(cards))
            return 6;
        else if (straight(cards))
            return 5;
        else if (threeOfAKind(cards))
            return 4;
        else if (twoPairs(cards))
            return 3;
        else if (pair(cards))
            return 2;
        else return 1;
    }

    public static int highCard(List<Card> cards) {
        cards.sort(new Comparator<Card>() {
            @Override
            public int compare(Card card1, Card card2) {
                return card1.getRank().ordinal() - card2.getRank().ordinal();
            }
        });

        return cards.get(cards.size() - 1).getRank().ordinal();
    }

    public static boolean pair(List<Card> cards) {
        int highest = -1;
        for (int i = 0; i < cards.size(); i++) {
            for (int j = i + 1; j < cards.size(); j++) {
                if (cards.get(i).getRank() == cards.get(j).getRank()) {
                    highest = max(max(highest, cards.get(i).getRank().ordinal()), cards.get(j).getRank().ordinal());
                }
            }
        }
        return highest != -1;
    }

    public static boolean twoPairs(List<Card> cards) {
//        int highest = -1;
        int[] ranks = new int[cards.size()];
        for (int i = 0; i < cards.size(); i++) {
            ranks[i] = cards.get(i).getRank().ordinal();
        }
        Arrays.sort(ranks);

        for (int i = 0; i < cards.size(); i++) {
            for (int j = i + 1; j < cards.size(); j++) {
                for (int k = j + 1; k < cards.size(); k++) {
                    for (int q = k + 1; q < cards.size(); q++) {
                        if (cards.get(i).getRank() == cards.get(j).getRank() && cards.get(k).getRank() == cards.get(q).getRank()) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean threeOfAKind(List<Card> cards) {
        int[] ranks = new int[cards.size()];
        for (int i = 0; i < cards.size(); i++) {
            ranks[i] = cards.get(i).getRank().ordinal();
        }
        Arrays.sort(ranks);
        if ((ranks[0] == ranks[1] && ranks[1] == ranks[2]) ||
                (ranks[1] == ranks[2] && ranks[2] == ranks[3]) ||
                (ranks[2] == ranks[3] && ranks[3] == ranks[4]) ||
                (ranks[3] == ranks[4] && ranks[4] == ranks[5]) ||
                (ranks[4] == ranks[5] && ranks[5] == ranks[6])) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean straight(List<Card> cards) {
        int[] ranks = new int[cards.size()];
        for (int i = 0; i < cards.size(); i++) {
            ranks[i] = cards.get(i).getRank().ordinal();
        }
        Arrays.sort(ranks);

        if ((ranks[0] + 1 == ranks[1] && ranks[1] + 1 == ranks[2] && ranks[2] + 1 == ranks[3] && ranks[3] + 1 == ranks[4]) ||
                (ranks[1] + 1 == ranks[2] && ranks[2] + 1 == ranks[3] && ranks[3] + 1 == ranks[4] && ranks[4] + 1 == ranks[5]) ||
                (ranks[2] + 1 == ranks[3] && ranks[3] + 1 == ranks[4] && ranks[4] + 1 == ranks[5] && ranks[5] + 1 == ranks[6])) {
            return true;
        }
        return false;
    }

    public static boolean flush(List<Card> cards) {
        int[] ranks = new int[cards.size()];
        for (int i = 0; i < cards.size(); i++) {
            ranks[i] = cards.get(i).getSuit().ordinal();
        }
        Arrays.sort(ranks);
        if (ranks[0] == ranks[1] && ranks[1] == ranks[2] && ranks[2] == ranks[3] && ranks[3] == ranks[4] ||
                ranks[1] == ranks[2] && ranks[2] == ranks[3] && ranks[3] == ranks[4] && ranks[4] == ranks[5] ||
                ranks[2] == ranks[3] && ranks[3] == ranks[4] && ranks[4] == ranks[5] && ranks[5] == ranks[6]) {
            return true;
        }
        return false;
    }

    public static boolean fullHouse(List<Card> cards) {
        if (!threeOfAKind(cards)) {
            return false;
        }

        int[] ranks = new int[cards.size()];
        for (int i = 0; i < cards.size(); i++) {
            ranks[i] = cards.get(i).getRank().ordinal();
        }
        Arrays.sort(ranks);

        for (int i = 0; i < cards.size(); i++) {
            for (int j = i + 1; j < cards.size(); j++) {
                for (int k = j + 1; k < cards.size(); k++) {
                    for (int q = k + 1; q < cards.size(); q++) {
                        for (int w = q + 1; w < cards.size(); w++) {
                            if (cards.get(i).getRank() == cards.get(j).getRank() &&
                                    cards.get(j).getRank() == cards.get(k).getRank() &&
                                    cards.get(q).getRank() == cards.get(w).getRank()) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean isStraight(int[] ranks, int pos) {
        for (int i = 0; i < 5; i++) {
            if (ranks[pos] + i != ranks[pos + i]) return false;
        }
        return true;
    }

    public static boolean isFlush(int[] suits, int pos) {
        for (int i = 0; i < 5; i++) {
            if (suits[pos] != suits[pos + i]) return false;
        }
        return true;
    }

    public static boolean fourOfAKind(List<Card> cards) {
        int[] ranks = new int[cards.size()];
        for (int i = 0; i < cards.size(); i++) {
            ranks[i] = cards.get(i).getRank().ordinal();
        }
        Arrays.sort(ranks);

        for (int i = 0; i < cards.size() - 3; i++) {
            boolean flag = true;
            for (int j = 0; j < 3; j++) {
                if (ranks[i] != ranks[i + j]) flag = false;
            }
            if (flag) return true;
        }
        return false;
    }

    public static boolean straightFlush(List<Card> cards) {
        cards.sort(new Comparator<Card>() {
            @Override
            public int compare(Card a, Card b) {
                if (a.getRank() != b.getRank()) {
                    return a.getRank().ordinal() - b.getRank().ordinal();
                } else {
                    return a.getSuit().ordinal() - b.getSuit().ordinal();
                }
            }
        });

        int[] ranks = new int[cards.size()];
        int[] suits = new int[cards.size()];
        for (int i = 0; i < cards.size(); i++) {
            ranks[i] = cards.get(i).getRank().ordinal();
            suits[i] = cards.get(i).getSuit().ordinal();
        }

        for (int i = 0; i < 3; i++) {
            if (isStraight(ranks, i) && isFlush(suits, i)) {
                return true;
            }
        }

        return false;
    }

    public static boolean royalFlush(List<Card> cards) {
        cards.sort(new Comparator<Card>() {
            @Override
            public int compare(Card a, Card b) {
                if (a.getRank() != b.getRank()) {
                    return a.getRank().ordinal() - b.getRank().ordinal();
                } else {
                    return a.getSuit().ordinal() - b.getSuit().ordinal();
                }
            }
        });

        int[] ranks = new int[cards.size()];
        int[] suits = new int[cards.size()];
        for (int i = 0; i < cards.size(); i++) {
            ranks[i] = cards.get(i).getRank().ordinal();
            suits[i] = cards.get(i).getSuit().ordinal();
        }

        for (int i = 0; i < 3; i++) {
            if (isStraight(ranks, i) && isFlush(suits, i) && ranks[i] == 8) {
                return true;
            }
        }

        return false;
    }

    public static String getWinningCase(int rank) {
        switch (rank) {
            case (1):
                return "High Card";
            case (2):
                return "Pair";
            case (3):
                return "Two pairs";
            case (4):
                return "Three of a kind";
            case (5):
                return "Straight";
            case (6):
                return "Flush";
            case (7):
                return "Full House";
            case (8):
                return "Four of a kind";
            case (9):
                return "Straight flush";
            case (10):
                return "Royal flush";
            default:
                return "Unknown";
        }
    }
}
