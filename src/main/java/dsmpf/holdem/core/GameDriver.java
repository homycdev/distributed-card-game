package dsmpf.holdem.core;

import dsmpf.holdem.network.GameEventServer;
import dsmpf.holdem.network.NodeIdentityManager;
import kotlin.Pair;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class GameDriver {
    private GameEventServer gameEventServer;
    private GameContext gameContext;

    public GameDriver(GameEventServer gameEventServer, GameContext gameContext) {
        this.gameEventServer = gameEventServer;
        this.gameContext = gameContext;
    }

    public void run() {
        GameEvent.Response response = null;
        GameEvent.ActionResponse actRes = null;
        do {
            gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, "New Game started!"));

            while (gameContext.currentStage != GameStage.END) {
                switch (gameContext.currentStage) {
                    case PREFLOP:
                        preFlop();
                        break;
                    case PREFLOP_BET:
                        bettingPhase();
                        break;
                    case FLOP:
                        interStage(3);
                        break;
                    case FLOP_BET:
                        bettingPhase();
                        break;
                    case TURN:
                        interStage(1);
                        break;
                    case TURN_BET:
                        bettingPhase();
                        break;
                    case RIVER:
                        interStage(1);
                        break;
                    case RIVER_BET:
                        bettingPhase();
                        break;
                    case SHOWDOWN:
                        showdown();
                        break;
                }
                gameContext.moveToNextStage();
                gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, "Current Stage: " + gameContext.currentStage.toString()));
                if (gameContext.bettingRoundContext.getCurrentPlayers().size() == 1) {
                    System.out.println("Number of players decreased to 1");
                    break;
                } else {
                    System.out.println("Number of active players: " + gameContext.bettingRoundContext.getCurrentPlayers().size());
                }
            }
            gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, "Game is over"));
            response = gameEventServer.publish(NodeIdentityManager.getSelfIdentity(), new GameEvent(GameEvent.Command.ACTION, "Do you want to play again? (Y/N):"));
            actRes = (GameEvent.ActionResponse) response;
            this.gameContext = new GameContext(this.gameContext.getPlayers());
        } while (actRes != null && (actRes.message.equals("Y") || actRes.message.equals("y")));
        System.out.println("\nGood bye!\n");
    }

    private void showdown() {

        for (var player : gameContext.getPlayers()) {
            var cards = new ArrayList<Card>();
            var playerCards = player.getCards();
            cards.add(playerCards.getFirst());
            cards.add(playerCards.getSecond());

            var playerCardsInfo = getCardsListInfo(cards, player.getId() + "\'s cards: ");

            gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, playerCardsInfo));
            int rank = getPlayerRank(player, gameContext.getCardsOnTheTable());
            int rank2 = getPlayerHighCardRank(player, gameContext.getCardsOnTheTable());

            player.setRank(rank);
            player.setHighRank(rank2);
        }

        var players = gameContext.getPlayers();
        players.sort(new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2) {
                if (p1.getRank() != p2.getRank()) {
                    return p1.getRank() - p2.getRank();
                } else {
                    return p1.getHighRank() - p2.getHighRank();
                }
            }
        });

        var winners = new ArrayList<Player>();
        var winner = players.get(players.size() - 1);

        for (var player : players) {
            if (player.getRank() == winner.getRank() && player.getHighRank() == winner.getHighRank()) {
                winners.add(player);
            }
        }

        int totalPot = gameContext.bettingRoundContext.getCurrentPot();
        int forOne = totalPot / winners.size();
        for (var winPlayer : winners) {
            gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, "Winner: " + winPlayer.getId()));
            winPlayer.setFund(winPlayer.getFund() + forOne);
        }
        gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, "Winners get: " + forOne + " funds"));
        var winningCase = CardRanker.getWinningCase(winner.getRank());
        gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, "Winning case: " + winningCase));
    }

    private List<Card> getAllCards(Player player, List<Card> cardsOnTheTable) {
        ArrayList<Card> cards = new ArrayList<>();
        cards.addAll(cardsOnTheTable);
        cards.add(player.getCards().getFirst());
        cards.add(player.getCards().getSecond());
        return cards;
    }

    private int getPlayerHighCardRank(Player player, List<Card> cardsOnTheTable) {
        var cards = getAllCards(player, cardsOnTheTable);
        return CardRanker.highCard(cards);
    }

    private int getPlayerRank(Player player, List<Card> cardsOnTheTable) {
        var cards = getAllCards(player, cardsOnTheTable);
        return CardRanker.rank(cards);
    }

    public void preFlop() {
        ArrayList<Pair<Card, Card>> cards = gameContext.cardManager.getInital();
        var players = gameContext.getPlayers();
        for (int i = 0; i < cards.size(); i++) {

            players.get(i).setCards(cards.get(i));

            var temp = new ArrayList<Card>();
            temp.add(cards.get(i).getFirst());
            temp.add(cards.get(i).getSecond());
            String cardInfo = getCardsListInfo(temp, "Your cards");

            var response = this.gameEventServer.publish(
                    gameContext.getPlayerId(i),
                    new GameEvent(GameEvent.Command.INFO, cardInfo)
            );

            if (response.type != GameEvent.Response.Type.Info) {
                System.out.println("Received unexpected respose to initial distribution message: " + response.toString());
            }
        }
    }

    public void interStage(int n) {
        ArrayList<Card> cards = gameContext.cardManager.getCards(n);
        String cardsInfo = getCardsListInfo(cards, "New cards");
        gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, cardsInfo));

        gameContext.addCardsToTheTable(cards);

        var allCards = gameContext.getCardsOnTheTable();
        cardsInfo = getCardsListInfo(allCards, "Cards on the table");
        gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, cardsInfo));
    }

    public void bettingPhase() {

        gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, "\nBetting stage started..."));

        final String bettingInfo = "Raise - raise <sum>, Call - call, Check - check, Fold - fold";

//        var players = gameContext.getPlayers();
//        BettingRoundContext bettingRoundContext = new BettingRoundContext(players);
        var bettingRoundContext = gameContext.bettingRoundContext;
        bettingRoundContext.reset();

        BettingRoundAction action = BettingRoundAction.FOLD;
        Object argument = null;
        boolean shouldProceed = true;

        do {
            if (bettingRoundContext.getCurrentPlayers().size() == 1) {
                System.out.println("Only one player left. The game cannot be continued.");
                break;
            }

            var player = bettingRoundContext.getCurrentPlayer();
            String playerFunds = "Your current fund: " + player.getFund();
            String currentBetInfo = "Current bet: " + bettingRoundContext.getCurrentBet();
            String totalPotOnTheTable = "Total pot on the table: " + bettingRoundContext.getCurrentPot();
            String info = "\n" + playerFunds + "\n" + currentBetInfo + "\n" + totalPotOnTheTable + "\n" + bettingInfo + "\n";
            var response = gameEventServer.publish(player.getId(), new GameEvent(GameEvent.Command.ACTION, info));

            if (response.type != GameEvent.Response.Type.Action) {
                if (response.type == GameEvent.Response.Type.ClientError) {
                    System.out.println(player.getId() + " has crashed. Removing it from the game.");
                    bettingRoundContext.removeCurrentPlayer();
                } else {
                    gameEventServer.publish(player.getId(), new GameEvent(GameEvent.Command.INFO, "Wrong type of command. Retry"));
                }
                continue;
            } else {
                GameEvent.ActionResponse actRes = (GameEvent.ActionResponse) response;
                String[] args = actRes.message.split("\\s+");

                switch (args[0]) {
                    case "raise":
                        action = BettingRoundAction.RAISE;
                        argument = args[1];
                        break;
                    case "call":
                        action = BettingRoundAction.CALL;
                        break;
                    case "check":
                        action = BettingRoundAction.CHECK;
                        break;
                    case "fold":
                        action = BettingRoundAction.FOLD;
                        break;
                    default:
                        gameEventServer.publish(player.getId(), new GameEvent(GameEvent.Command.INFO, "Unkown action. Try again!"));
                        continue;
                }
                gameEventServer.publishSequentially(new GameEvent(GameEvent.Command.INFO, player.getId() + " - " + args[0] + " " + (args.length > 1 ? args[1] : "")));
            }

            try {
                shouldProceed = bettingRoundContext.applyCurrentActorAction(action, argument);
            } catch (InvalidParameterException e) {
                gameEventServer.publish(player.getId(), new GameEvent(GameEvent.Command.INFO, e.toString()));
                continue;
            }

        } while (shouldProceed);
    }

    private String getCardsListInfo(List<Card> cards, String footer) {
        final String left = "\n" + footer + "\n\t[";

        String body = "";
        for (var card : cards) {
            body += CardManager.getCardInfo(card) + ", ";
        }

        body = body.substring(0, body.length() - 2);
        String cardInfo = left + body + "]\n";
        return cardInfo;
    }

}
