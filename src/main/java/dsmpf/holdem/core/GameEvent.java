package dsmpf.holdem.core;

import java.io.Serializable;
import java.util.UUID;

public class GameEvent implements Serializable {

    public enum Command {
        EXIT,
        INFO,
        ACTION,
    }

    public Command command;
    public String message;

    public GameEvent() {
    }

    public GameEvent(Command command, String message) {
        this.command = command;
        this.message = message;
    }

    @Override
    public String toString() {
        return "GameEvent{" +
                "command=" + command +
                ", message='" + message + '\'' +
                '}';
    }

    public static class Response implements Serializable {
        public enum Type {
            ClientError,
            Action,
            Info,
        }

        public Type type;
        public UUID respondent;

        public Response withRespondent(UUID nodeId) {
            this.respondent = nodeId;
            return this;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "type=" + type +
                    '}';
        }
    }

    public static class InfoResponse extends Response {
        public String message;

        public InfoResponse(String message) {
            this.message = message;
            this.type = Type.Info;
        }

        @Override
        public String toString() {
            return "InfoResponse{" +
                    "type=" + type +
                    ", message='" + message + '\'' +
                    '}';
        }
    }

    public static class ClientErrorResponse extends Response {
        public Exception exception;

        public ClientErrorResponse(Exception exception) {
            this.exception = exception;
            this.type = Type.ClientError;
        }

        @Override
        public String toString() {
            return "ClientErrorResponse{" +
                    "type=" + type +
                    ", exception=" + exception +
                    '}';
        }
    }

    public static class ActionResponse extends Response {
        public String message;

        public ActionResponse(String message) {
            this.message = message;
            this.type = Type.Action;
        }

        @Override
        public String toString() {
            return "ActionResponse{" +
                    "type=" + type +
                    ", message='" + message + '\'' +
                    '}';
        }
    }

}
