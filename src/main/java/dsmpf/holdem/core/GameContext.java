package dsmpf.holdem.core;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GameContext {

    public GameStage currentStage = GameStage.BEGIN;
    private List<Player> allPlayers;
    private List<Player> currentPlayers;
    public CardManager cardManager;
    private List<Card> cardsOnTheTable;
    public BettingRoundContext bettingRoundContext;

    public GameContext(List<Player> players) {
        this.allPlayers = new ArrayList<>();
        this.currentPlayers = new ArrayList<>();

        this.allPlayers.addAll(players);
        this.currentPlayers.addAll(players);

        this.cardsOnTheTable = new ArrayList<>();

        this.bettingRoundContext = new BettingRoundContext(this.allPlayers);

        cardManager = new CardManager(this.allPlayers.size());
    }

    public UUID getPlayerId(int index) {
        return this.currentPlayers.get(index).getId();
    }

    public List<Player> getPlayers() {
        return this.currentPlayers;
    }

    public void moveToNextStage() {
        this.currentStage = this.currentStage.getNextStage(this.currentStage);
    }

    public void addCardsToTheTable(List<Card> newCards) {
        this.cardsOnTheTable.addAll(newCards);
    }

    public List<Card> getCardsOnTheTable() {
        return this.cardsOnTheTable;
    }
}
