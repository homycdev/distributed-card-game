package dsmpf.holdem.core;

import kotlin.Pair;

import java.util.UUID;

public class Player {
    private UUID id;
    private int funds = 1000;
    Pair<Card, Card> cards;
    int cardRank = 0;
    int cardHighRank = 0;

    public Player(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return this.id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setFund(int fund) {
        this.funds = fund;
    }

    public int getFund() {
        return this.funds;
    }

    public void setCards(Pair<Card, Card> cards) {
        this.cards = new Pair<>(cards.getFirst(), cards.getSecond());
    }

    public Pair<Card, Card> getCards() {
        return this.cards;
    }

    public void setRank(int rank) {
        this.cardRank = rank;
    }

    public int getRank() {
        return this.cardRank;
    }

    public void setHighRank(int rank2) {
        this.cardHighRank = rank2;
    }

    public int getHighRank() {
        return this.cardHighRank;
    }
}
