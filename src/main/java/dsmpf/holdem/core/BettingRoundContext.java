package dsmpf.holdem.core;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;

public class BettingRoundContext {

    int cycle = 0;
    int totalPot = 0;
    int currentBet = 0;
    Player currentActor;
    int pos = 0;
    List<Player> playerList;
    int[] betList;

    public BettingRoundContext(List<Player> playerList) {
        this.playerList = playerList;
        this.betList = new int[playerList.size()];
        this.currentActor = this.playerList.get(0);
        Arrays.fill(this.betList, 0);
    }

    public boolean applyCurrentActorAction(BettingRoundAction action, Object args) {
        switch (action) {
            case RAISE: {
                int newBet = Integer.parseInt((String) args);
                if (currentBet > newBet) {
                    throw new InvalidParameterException("Raise value is smaller than current bid");
                }
                currentBet = newBet;
                if (currentActor.getFund() < currentBet) {
                    throw new InvalidParameterException("Not enough funds");
                }
                currentActor.setFund(currentActor.getFund() - currentBet);
                totalPot += currentBet;
                betList[pos] = currentBet;
                // invalidate players bets, require call or fold action

                break;
            }
            case CALL: {
                if (currentActor.getFund() < currentBet) {
                    throw new InvalidParameterException("Not enough funds");
                }
                currentActor.setFund(currentActor.getFund() - currentBet);
                totalPot += currentBet;
                betList[pos] = currentBet;

                // put player bet into the pot
            }
            case CHECK: {
                break;
            }
            case FOLD: {
                // remove player from list
                this.playerList.remove(pos);
            }
        }
        pos = (pos + 1) % this.playerList.size();
        this.currentActor = this.playerList.get(pos);
        if (pos == 0) {
            if (currentBet == betList[0]) {
                return false;
            }
        }
        return true;
    }

    public Player getCurrentPlayer() {
        return this.currentActor;
    }

    public int switchToNextActor() {
        var doSwitch = false;
        for (var player : playerList) {
            if (doSwitch) {
                // set player as an actor
                doSwitch = false;
                currentActor = player;
                break;
            }
            if (player == currentActor) {
                // mark next player in list to be an actor
                doSwitch = true;
            }
        }
        if (doSwitch) {
            // if list have ended with doSwitch flag, start new betting cycle
            cycle++;
            currentActor = playerList.get(0);
        }
        return cycle;
    }

    public List<Player> getCurrentPlayers() {
        return playerList;
    }

    public void removeCurrentPlayer() {
        System.out.println("Removing...");
        this.playerList.remove(pos);
        pos = pos % this.playerList.size();
        this.currentActor = this.playerList.get(pos);
    }

    public void reset() {
        this.currentBet = 0;
        Arrays.fill(this.betList, 0);
        this.currentActor = this.playerList.get(0);
        this.pos = 0;
    }

    public int getCurrentBet() {
        return this.currentBet;
    }

    public int getCurrentPot() {
        return this.totalPot;
    }
}
