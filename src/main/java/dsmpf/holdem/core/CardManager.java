package dsmpf.holdem.core;

import kotlin.Pair;

import java.util.ArrayList;
import java.util.Random;

public class CardManager {
    private int nPlayers;
    private int totalNumOfCards;
    private ArrayList<Card> cards;
    private final Random randomGenerator = new Random(0);


    public CardManager(int numPlayers) {
        this.nPlayers = numPlayers;
        this.cards = new ArrayList<>();
        for (var rank : Card.Rank.values()) {
            for (var suit : Card.Suit.values()) {
                this.cards.add(new Card(rank, suit));
            }
        }
        this.totalNumOfCards = this.cards.size();
    }

    public ArrayList<Pair<Card, Card>> getInital() {
        var distribution = new ArrayList<Pair<Card, Card>>();
        for (int i = 0; i < nPlayers; i++) {
            var cardId = randomGenerator.nextInt(totalNumOfCards - 2 * i);
            Card firstCard = cards.get(cardId);
            cards.remove(cardId);

            cardId = randomGenerator.nextInt(totalNumOfCards - 2 * i);
            Card secondCard = cards.get(cardId);
            cards.remove(cardId);

            distribution.add(new Pair<>(firstCard, secondCard));
        }
        return distribution;
    }

    public ArrayList<Card> getCards(int count) {
        ArrayList<Card> res = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            var cardId = randomGenerator.nextInt(cards.size());
            res.add(cards.get(cardId));
            cards.remove(cardId);
        }
        return res;
    }

    public static String getCardInfo(Card card) {
        String[] Ranks = new String[]{"2", "3", "4", "5", "6", "7", "8", "9", "10", "JACK", "QUEEN", "KING", "ACE"};
        String[] Suits = new String[]{"DIAMONDS", "HEARTS", "SPADES", "CLUBS"};
        int rank = card.getRank().ordinal();
        int suit = card.getSuit().ordinal();
        return Ranks[rank] + " " + Suits[suit];
    }

}
