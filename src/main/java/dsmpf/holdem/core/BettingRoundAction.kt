package dsmpf.holdem.core


enum class BettingRoundAction {
    RAISE, CALL, CHECK, FOLD
}