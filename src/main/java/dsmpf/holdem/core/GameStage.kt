package dsmpf.holdem.core

enum class GameStage {
    BEGIN, ANTS, BLINDS, PREFLOP, PREFLOP_BET, FLOP, FLOP_BET, TURN, TURN_BET, RIVER, RIVER_BET, SHOWDOWN, END;

    fun getNextStage(currentStage: GameStage): GameStage {
        // do not uses blinds and ants
        return when (currentStage) {
            BEGIN, BLINDS, ANTS -> PREFLOP
            PREFLOP -> PREFLOP_BET
            PREFLOP_BET -> FLOP
            FLOP -> FLOP_BET
            FLOP_BET -> TURN
            TURN -> TURN_BET
            TURN_BET -> RIVER
            RIVER -> RIVER_BET
            RIVER_BET -> SHOWDOWN
            SHOWDOWN -> END
            else -> END
        }
    }

    fun isBettingStage(stage: GameStage): Boolean {
        return when (stage) {
            PREFLOP_BET, FLOP_BET, TURN_BET, RIVER_BET -> true
            else -> false
        }
    }
}