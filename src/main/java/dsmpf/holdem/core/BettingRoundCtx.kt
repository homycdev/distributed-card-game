package dsmpf.holdem.core


class BettingRoundCtx {
    val cycle: Int = 0
    val totalPlot: Int = 0
    val currentBet: Int = 0
    lateinit var playerList: List<Player>

    constructor(playerList: List<Player>) {
        this.playerList = playerList
    }

    fun applyCurrentActorAction(action: BettingRoundAction, args: Any) {
        when (action) {
            BettingRoundAction.RAISE -> currentBet.plus(args as Long)
            BettingRoundAction.CALL -> println("Put player bet into the pot")
            BettingRoundAction.FOLD -> println("Player ")
            BettingRoundAction.CHECK -> println("Check")
        }
    }


}