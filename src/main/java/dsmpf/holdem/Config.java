package dsmpf.holdem;

import dsmpf.holdem.utils.ProgramArgumentParser.Value;

public class Config {

    @Value(name = "bootstrap-server-addr")
    String bootstrapServerAddr = null;

    @Value(name = "enable-client", argCount = 0)
    boolean enableClient;

    @Value(name = "advertised-address")
    String advertisedAddress = null;

    @Value(name = "disable-shutdown-hook", argCount = 0)
    boolean disableShutdownHook;

    @Value(name = "hp-tcp-port")
    int holePunchingServerTcpPort = 2111;

    @Value(name = "nat", argCount = 0)
    boolean isBehindNat = false;

    @Override
    public String toString() {
        return "Config{" +
                "bootstrapServerAddr='" + bootstrapServerAddr + '\'' +
                ", enableClient=" + enableClient +
                ", advertisedPublicAddress='" + advertisedAddress + '\'' +
                ", disableShutdownHook=" + disableShutdownHook +
                ", holePunchingServerTcpPort=" + holePunchingServerTcpPort +
                '}';
    }
}
