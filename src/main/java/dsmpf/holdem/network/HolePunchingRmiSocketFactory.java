package dsmpf.holdem.network;

import dsmpf.holdem.network.HolePunchingTcpService.HolePunchingTcpClientImpl;

import java.io.IOException;
import java.io.Serializable;
import java.net.*;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.util.UUID;

/**
 * This whole class will be serialized to make createSocket be available on the remote nodes
 */
public class HolePunchingRmiSocketFactory implements RMIClientSocketFactory, RMIServerSocketFactory, Serializable {

    private NodeIdentity nodeIdentity;
    private UUID id;
    private InetSocketAddress mediatorSocketAddress;
    private HolePunchingTcpService holePunchingTcpService;

    public HolePunchingRmiSocketFactory(NodeIdentity nodeIdentity) {
        this.nodeIdentity = nodeIdentity;
        this.id = nodeIdentity.id;
    }

    public static void printDebug(String string) {
        System.err.println(string);
    }

    public void setMediatorSocketAddress(InetSocketAddress mediatorSocketAddress) {
        this.mediatorSocketAddress = mediatorSocketAddress;
        try {
            this.holePunchingTcpService = new HolePunchingTcpClientImpl(mediatorSocketAddress);
            printDebug("New endpoint for hole punching = " + mediatorSocketAddress.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket createDirectSocket(String host, int port) {
        try {
            var directClient = new Socket();
            directClient.setReuseAddress(true);
            var directConnectionAddr = new InetSocketAddress(host, port);
            directClient.connect(directConnectionAddr);
            printDebug("Direct connection from "
                    + directClient.getLocalAddress().toString() + ":" + directClient.getLocalPort() +
                    " to " +
                    directConnectionAddr.toString()
                    + " is completed");
            return directClient;
        } catch (IOException e) {
            printDebug("Direct connection failed: " + e.getMessage());
            e.printStackTrace(System.err);
            return null;
        }
    }

    /**
     * Factory method provided to connect from remote endpoints to current node
     * Works on remote(client) nodes
     *
     * @param host factory owner server host
     * @param port factory owner server port
     * @return socket to communicate with server
     * @throws IOException if failed
     */
    @Override
    public Socket createSocket(String host, int port) throws IOException {
        System.out.println("creating new socket : " + host + ":" + port);
        //new NullPointerException().printStackTrace(System.err);

        // try direct connection first
        Socket directSocket;
        if ((directSocket = createDirectSocket(host, port)) != null) {
            return directSocket;
        }

        // try tcp hole punching
        return holePunchingTcpService.connect(nodeIdentity.id);
    }

    /**
     * Factory method to create server socket of java rmi services
     * Called on server node, and usually once
     *
     * @param port port to listen rmi connections
     * @return server socket
     * @throws IOException if failed
     */
    @Override
    public ServerSocket createServerSocket(int port) throws IOException {
        printDebug("creating new server socket at :" + port);
        //new NullPointerException().printStackTrace(System.err);
        HolePunchingServerSocket holePunchingServerSocket = new HolePunchingServerSocket(port, id);
        if (holePunchingTcpService != null) {
            printDebug("  with usage of tcp hole punching service");
            holePunchingServerSocket.useHolePunchingService(holePunchingTcpService);
        }
        // listen is done later by rmi
        return holePunchingServerSocket;
    }

    private static class HolePunchingServerSocket extends ServerSocket {

        private int port;
        private UUID id;
        private HolePunchingTcpService holePunchingTcpService;
        private Socket mediatorClient;

        public HolePunchingServerSocket(int port, UUID id) throws IOException {
            super(port);
            this.port = port;
            this.id = id;
        }

        public void useHolePunchingService(HolePunchingTcpService holePunchingTcpService) {
            holePunchingTcpService.register(id, port);
        }

        @Override
        public void bind(SocketAddress endpoint, int backlog) throws IOException {
            super.bind(endpoint, backlog);
            printDebug("server socket is set to listen " + this.getLocalSocketAddress().toString());
        }

        //        public Socket accept() throws IOException {
//            //var superSocket = super.accept();
//            var superSocket = new Socket();
//            super.implAccept(superSocket);
//            System.out.println("accepted socket : local="+superSocket.getLocalAddress().toString()+", remote="+superSocket.getInetAddress().toString());
//            return superSocket;
//        }
    }

    private static class HolePuncher {

        public Socket doHolePunch(InetSocketAddress remoteInetSocketAddr,
                                  InetSocketAddress localInetSocketAddr,
                                  int ownPort) throws ConnectException {
            return null;
        }
    }

}
