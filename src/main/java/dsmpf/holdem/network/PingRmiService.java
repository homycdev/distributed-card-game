package dsmpf.holdem.network;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PingRmiService extends Remote {

    String NAME = "PingService";

    String ping() throws RemoteException;

    class PingRmiServiceImpl implements PingRmiService {

        private final String rmiServiceName;

        public PingRmiServiceImpl(String rmiServiceName) {
            this.rmiServiceName = rmiServiceName;
        }

        @Override
        public String ping() throws RemoteException {
            System.out.println("ping received");
            return "pong";
        }

        public static PingRmiServiceImpl register(RmiServiceDiscovery rmiServiceDiscovery, String name) throws RemoteException, AlreadyBoundException {
            var pingRmiService = new PingRmiServiceImpl(name);
            return rmiServiceDiscovery.register(name, pingRmiService);
        }

        public void unregister(RmiServiceDiscovery rmiServiceDiscovery) throws RemoteException, NotBoundException {
            rmiServiceDiscovery.unregister(rmiServiceName);
        }
    }

}
