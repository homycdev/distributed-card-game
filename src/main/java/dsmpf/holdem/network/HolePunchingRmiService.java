package dsmpf.holdem.network;

import dsmpf.holdem.network.HolePunchingTcpService.HolePunchingTcpServiceImpl;

import java.io.Serializable;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Used as external endpoint to connect two peers
 */
public interface HolePunchingRmiService extends Remote {

    String NAME = "HolePunchingService";

    /**
     * Called by peers to obtain tcp hole puncher service port
     *
     * @return port of hole puncher, which is used by rmi socket factory
     */
    int getHolePuncherPort() throws RemoteException;

    interface HolePunchListener extends Serializable, Remote {
        boolean onHolePunchRequest(UUID clientNodeUUID) throws RemoteException;
    }

    class HolePunchingRmiServiceImpl implements HolePunchingRmiService {
        private final String rmiServiceName;
        private final Logger logger;
        private final int holePuncherPort;
        private HolePunchingTcpServiceImpl tcpHolePunchingServer;

        public HolePunchingRmiServiceImpl(String rmiServiceName, int holePuncherPort) {
            this.rmiServiceName = rmiServiceName;
            this.logger = Logger.getLogger(rmiServiceName);
            this.holePuncherPort = holePuncherPort;
            this.tcpHolePunchingServer = new HolePunchingTcpServiceImpl(holePuncherPort);
            logger.info("HolePunching services is now running on this node");
        }

        @Override
        public int getHolePuncherPort() {
            return holePuncherPort;
        }


        public static HolePunchingRmiServiceImpl register(RmiServiceDiscovery rmiServiceDiscovery,
                                                          String name, int tcpHolePuncherPort)
                throws RemoteException, AlreadyBoundException {
            var holePunchingRmiService = new HolePunchingRmiServiceImpl(name, tcpHolePuncherPort);
            return rmiServiceDiscovery.register(name, holePunchingRmiService);
        }

        public void unregister(RmiServiceDiscovery rmiServiceDiscovery) throws RemoteException, NotBoundException {
            tcpHolePunchingServer.stop();
            rmiServiceDiscovery.unregister(rmiServiceName);
        }


    }

}
