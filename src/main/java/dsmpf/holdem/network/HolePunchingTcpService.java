package dsmpf.holdem.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public interface HolePunchingTcpService {

    public static int COMMAND_REGISTER = 1;
    public static int COMMAND_CONNECT = 2;

    public static class CompoundEndpoint implements Serializable {
        // node uuid
        private UUID nodeId;
        // local address + local port of connection to mediator
        private InetSocketAddress innerAddr;
        // (address of client + port of client connection) from mediator point of view
        private InetSocketAddress outerAddr;

        public CompoundEndpoint(UUID nodeId, InetSocketAddress innerAddr, InetSocketAddress outerAddr) {
            this.nodeId = nodeId;
            this.innerAddr = innerAddr;
            this.outerAddr = outerAddr;
        }
    }

    /**
     * This method is used to notify hole puncher about our existence via tcp
     *
     * @param serverNodeID    current node id
     * @param localListenPort port we would like to provide for tcp callbacks
     * @return listening socket to read connection attempts from it
     */
    Socket register(UUID serverNodeID, int localListenPort);

    /**
     * Called by client peer to initiate hole punching via
     *
     * @param targetNodeUUID id of node we would like to connect to
     * @return socket to the target endpoint
     */
    Socket connect(UUID targetNodeUUID);

    /**
     * Client side implementation of TCP Hole Puncher
     */
    public class HolePunchingTcpClientImpl implements HolePunchingTcpService {

        private final InetSocketAddress holePuncherAddress;

        public HolePunchingTcpClientImpl(InetSocketAddress holePuncherAddress) throws IOException {
            this.holePuncherAddress = holePuncherAddress;
        }

        @Override
        public Socket register(UUID serverNodeID, int localListenPort) {
            try {
                var holePuncherCallbackListener = new Socket();
                holePuncherCallbackListener.setReuseAddress(true);
                holePuncherCallbackListener.bind(new InetSocketAddress(localListenPort));
                holePuncherCallbackListener.connect(holePuncherAddress);

                var out = new ObjectOutputStream(holePuncherCallbackListener.getOutputStream());
                var serverEndpoint = new CompoundEndpoint(
                        serverNodeID,
                        new InetSocketAddress(InetAddress.getLocalHost(), holePuncherCallbackListener.getLocalPort()),
                        null
                );
                out.writeInt(COMMAND_REGISTER);
                out.writeObject(serverEndpoint);
                out.flush();
                var in = new ObjectInputStream(holePuncherCallbackListener.getInputStream());
                if (in.readBoolean()) {
                    return holePuncherCallbackListener;
                } else {
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace(System.err);
                return null;
            }
        }

        @Override
        public Socket connect(UUID targetNodeUUID) {
            try {
                var holePuncherClient = new Socket();
                holePuncherClient.setReuseAddress(true);
                try {
                    holePuncherClient.connect(holePuncherAddress);
                } catch (IOException e) {
                    throw new IOException("Unable to connect to TcpHolePunchingService at " + holePuncherAddress.toString());
                }
                var out = new ObjectOutputStream(holePuncherClient.getOutputStream());
                var requestedCompoundEndpoint = new CompoundEndpoint(
                        targetNodeUUID,
                        new InetSocketAddress(InetAddress.getLocalHost(), holePuncherClient.getLocalPort()),
                        null
                );
                out.writeInt(COMMAND_CONNECT);
                out.writeObject(requestedCompoundEndpoint);

                var in = new ObjectInputStream(holePuncherClient.getInputStream());
                CompoundEndpoint fullTargetEndpoint = null;
                try {
                    fullTargetEndpoint = (CompoundEndpoint) in.readObject();
                } catch (ClassNotFoundException e) {
                    throw new IOException("Unable to fetch full target endpoint due to " + e.getMessage());
                }
                if (fullTargetEndpoint == null) {
                    throw new IOException("Unable to connect to the service : tcp hole punching is not available");
                }

                // run hole punching

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return null;
        }
    }

    /**
     * Server side implementation of TCP Hole Puncher
     */
    public class HolePunchingTcpServiceImpl {

        private ServerSocket serverSocket;
        private Thread serviceThread;
        private Logger logger = Logger.getLogger("TcpHolePunchingService");
        private ExecutorService threadPool = Executors.newCachedThreadPool();
        private Map<CompoundEndpoint, Socket> endpointMap = new HashMap<>();

        public HolePunchingTcpServiceImpl(int port) {
            try {
                serverSocket = new ServerSocket(port);
                serviceThread = new Thread(this::serviceRun);
                serviceThread.start();
                logger.info("TcpHolePunchingService is running on port " + port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public CompoundEndpoint findEndpointByNodeId(UUID nodeId) {
            for (var endpoint : endpointMap.keySet()) {
                if (endpoint.nodeId.equals(nodeId)) {
                    return endpoint;
                }
            }
            logger.info("Client has requested connection to node " + nodeId + ", but it is not registered to punch holes");
            return null;
        }

        public void serviceRun() {
            while (!serverSocket.isClosed()) {
                try {
                    Socket socket = serverSocket.accept();
                    CompletableFuture.runAsync(serveConnection(socket), threadPool);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public Runnable serveConnection(Socket socket) {
            return () -> {
                try {
                    ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                    ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                    int command = in.readInt();
                    switch (command) {
                        case COMMAND_REGISTER: { // register compound endpoint
                            var serverCompoundEndpoint = (CompoundEndpoint) in.readObject();
                            serverCompoundEndpoint.outerAddr = new InetSocketAddress(socket.getInetAddress(), socket.getPort());
                            endpointMap.put(serverCompoundEndpoint, socket);
                            out.writeBoolean(true);
                            out.flush();
                            break;
                        }
                        case COMMAND_CONNECT: { // request connection to server
                            var requestedEndpoint = (CompoundEndpoint) in.readObject();
                            var serverEndpoint = findEndpointByNodeId(requestedEndpoint.nodeId);
                            var clientEndpoint = new CompoundEndpoint(
                                    requestedEndpoint.nodeId,
                                    requestedEndpoint.innerAddr,
                                    new InetSocketAddress(socket.getInetAddress(), socket.getPort())
                            );

                            Socket serverTcpSocket = endpointMap.get(serverEndpoint);
                            try {
                                var servOut = new ObjectOutputStream(serverTcpSocket.getOutputStream());
                                servOut.writeObject(clientEndpoint);
                                servOut.flush();
                            } catch (Exception e) {
                                endpointMap.remove(serverEndpoint);
                                var cliOut = out;
                                cliOut.writeObject(null);
                                cliOut.flush();
                                socket.close();
                                break;
                            }
                            var cliOut = out;
                            cliOut.writeObject(serverEndpoint);
                            cliOut.flush();
                            in.readObject(); //TODO: remove?
                            in.close();
                            break;
                        }
                        default: {
                            throw new IOException("Unknown command");
                        }
                    }
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            };
        }

        public void stop() {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
