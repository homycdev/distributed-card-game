package dsmpf.holdem.network;

import java.io.Serializable;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Bootstrap service is running on machine with public IP address and used to coordinate others
 */
public interface RegistrationRmiService extends Remote {

    public static final String NAME = "RegistrationService";

    /**
     * Called by client to start up registration chain
     *
     * @param partialIdentity minimal identity of client
     * @return full identity
     * @throws RemoteException if registration failed
     */
    NodeIdentity registerNewNode(NodeIdentity partialIdentity) throws RemoteException;

    /**
     * Called by initial registrar to push new clients to other public nodes
     *
     * @param fullIdentity identity of the new node
     * @return true, if success
     * @throws RemoteException if propagation failed
     */
    boolean propagateNewNode(NodeIdentity fullIdentity) throws RemoteException;

    /**
     * Called by client to start receiving nodes list update callbacks
     *
     * @param clientIdentity       client identity, to map it
     * @param onNodeChangeListener listener to call on callbacks
     * @return true, if success
     */
    boolean addOnNodeChangeListener(NodeIdentity clientIdentity, OnNodeChangeListener onNodeChangeListener) throws RemoteException;

    /**
     * Called by client to stop receiving nodes list update callbacks
     *
     * @param clientIdentity       client identity, to find existing callback in the map
     * @param onNodeChangeListener listener to remove from map;
     * @return true, if success
     */
    boolean removeOnNodeChangeListener(NodeIdentity clientIdentity, OnNodeChangeListener onNodeChangeListener) throws RemoteException;

    List<NodeIdentity> getKnownNodesList() throws RemoteException;

    public static interface OnNodeChangeListener extends Remote, Serializable {
        void onNodeRegister(NodeIdentity newRegisteredNode) throws RemoteException;

        void onNodeUnregister(NodeIdentity removedNode) throws RemoteException;
    }

    public static class RegistrationRmiServiceImpl implements RegistrationRmiService {

        // private final Registry rmiRegistry;
        private final RmiServiceDiscovery rmiServiceDiscovery;
        private final Map<UUID, RegistrationRmiService> publicRegistrationServices;
        private final Map<UUID, OnNodeChangeListener> onNodeChangeListenerMap;
        private final String rmiServiceName;
        private final Logger logger;

        public RegistrationRmiServiceImpl(RmiServiceDiscovery rmiServiceDiscovery, String rmiServiceName) {
            this.publicRegistrationServices = new HashMap<>();
            this.onNodeChangeListenerMap = new HashMap<>();
            this.rmiServiceDiscovery = rmiServiceDiscovery;
            this.rmiServiceName = rmiServiceName;
            this.logger = Logger.getLogger(rmiServiceName);
            logger.info("Registration service is now running on this node");
        }

        @Override
        public NodeIdentity registerNewNode(NodeIdentity partialIdentity) throws RemoteException {

            //partialIdentity = NodeIdentityManager.register(partialIdentity);
            //TODO: real ip and set behind nat, without usage of register
            var fullIdentity = partialIdentity;

            // we, as public node, should notify other public nodes about changes via direct call
            var publicNodes = NodeIdentityManager.getIdentityList().stream()
                    .filter((nodeIdentity -> !nodeIdentity.isBehindNat))
                    .collect(Collectors.toList());
            for (var publicNode : publicNodes) {
                RegistrationRmiService registrationRmiService;
                if (publicRegistrationServices.containsKey(publicNode.id)) {
                    registrationRmiService = publicRegistrationServices.get(publicNode.id);
                } else {
                    try {
                        registrationRmiService = rmiServiceDiscovery.lookup(publicNode.id, RegistrationRmiService.NAME);
                    } catch (NotBoundException e) {
                        throw new RemoteException(e.toString());
                    }
                }
                if (!publicNode.id.equals(NodeIdentityManager.getSelfIdentity().id)) {
                    logger.info("Propagating new node change to public node " + publicNode.id);
                    registrationRmiService.propagateNewNode(fullIdentity);
                }
            }
            propagateNewNode(fullIdentity);
            return fullIdentity;
        }

        public boolean unregisterNode(NodeIdentity fullIdentity) throws RemoteException {
            //TODO: implement
            return false;
        }

        @Override
        public boolean propagateNewNode(NodeIdentity fullIdentity) throws RemoteException {
            // all public nodes are notified by our caller
            // now we, as public node, should notify nat nodes about changes via callbacks
            NodeIdentityManager.register(fullIdentity);
            var callbackList = onNodeChangeListenerMap.entrySet();
            for (var callback : callbackList) {
                logger.info("Executing callback on new node change to hidden node " + callback.getKey());
                callback.getValue().onNodeRegister(fullIdentity);
            }
            return false;
        }

        @Override
        public boolean addOnNodeChangeListener(NodeIdentity clientIdentity, OnNodeChangeListener onNodeChangeListener) {
            onNodeChangeListenerMap.put(clientIdentity.id, onNodeChangeListener);
            return true;
        }

        @Override
        public boolean removeOnNodeChangeListener(NodeIdentity clientIdentity, OnNodeChangeListener onNodeChangeListener) {
            if (!onNodeChangeListenerMap.containsKey(clientIdentity.id)) {
                return false;
            }
            onNodeChangeListenerMap.remove(clientIdentity.id);
            return true;
        }

        @Override
        public List<NodeIdentity> getKnownNodesList() {
            logger.fine("ask for getKnownNodesList");
            return NodeIdentityManager.getIdentityList();
        }

        public static RegistrationRmiServiceImpl register(RmiServiceDiscovery serviceDiscovery, String name) throws RemoteException, AlreadyBoundException {
            var bootstrapRmiService = new RegistrationRmiServiceImpl(serviceDiscovery, name);
            return serviceDiscovery.register(name, bootstrapRmiService);
        }

        public void unregister(RmiServiceDiscovery serviceDiscovery) throws RemoteException, NotBoundException {
            serviceDiscovery.unregister(rmiServiceName);
        }


    }
}
