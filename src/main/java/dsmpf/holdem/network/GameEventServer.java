package dsmpf.holdem.network;

import dsmpf.holdem.core.GameEvent;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class GameEventServer {
    /**
     * RMI service whose methods are invoked to subscibe nodes to game events
     */
    public interface Publisher extends Remote {
        String NAME = "GameEventPublisher";

        void addSubscriberListener(NodeIdentity nodeIdentity, GameEventClient.Listener listener) throws RemoteException;

        void removeSubscriberListener(NodeIdentity nodeIdentity, GameEventClient.Listener listener) throws RemoteException;

    }

    private RmiServiceDiscovery rmiServiceDiscovery;
    private Publisher rmiPublisher;
    private List<GameEventClient.Listener> listenerList;
    private Map<UUID, GameEventClient.Listener> listenerMap;
    private ExecutorService threadPool = Executors.newCachedThreadPool();

    public GameEventServer(RmiServiceDiscovery rmiServiceDiscovery) {
        this.rmiServiceDiscovery = rmiServiceDiscovery;
        listenerMap = new HashMap<>();
        listenerList = new ArrayList<>();
        rmiPublisher = new Publisher() {
            @Override
            public void addSubscriberListener(NodeIdentity nodeIdentity, GameEventClient.Listener listener) throws RemoteException {
                listenerList.add(listener);
                listenerMap.put(nodeIdentity.id, listener);
            }

            @Override
            public void removeSubscriberListener(NodeIdentity nodeIdentity, GameEventClient.Listener listener) throws RemoteException {
                listenerList.remove(listener);
                listenerMap.remove(nodeIdentity.id);
            }
        };
    }

    public boolean start() {
        try {
            rmiServiceDiscovery.register(Publisher.NAME, rmiPublisher);
            return true;
        } catch (RemoteException | AlreadyBoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean stop() {
        try {
            rmiServiceDiscovery.unregister(Publisher.NAME);
            return true;
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    /***
     * Invokes event subscriber and handles exceptions in case of failure wrapinng them into responses
     * @param listenerNodeId
     * @param listener
     * @param gameEvent
     * @return
     */
    private GameEvent.Response callListener(UUID listenerNodeId, GameEventClient.Listener listener, GameEvent gameEvent) {
        try {
            return listener.onGameEvent(gameEvent).withRespondent(listenerNodeId);
        } catch (RemoteException e) {
            return new GameEvent.ClientErrorResponse(e).withRespondent(listenerNodeId);
        }
    }

    /**
     * Spread game event sequentially to all subsribed nodes
     *
     * @param gameEvent
     * @return
     */
    public List<GameEvent.Response> publishSequentially(GameEvent gameEvent) {
        List<GameEvent.Response> list = new ArrayList<>(listenerList.size());
        for (var listenerEntry : listenerMap.entrySet()) {
            GameEvent.Response o = callListener(listenerEntry.getKey(), listenerEntry.getValue(), gameEvent);
            list.add(o);
        }
        return list;
    }

    /**
     * Does the same as publisSequentially, but concurrently.
     * Uses threadpool
     *
     * @param gameEvent
     * @return
     */
    public List<GameEvent.Response> publishAll(GameEvent gameEvent) {
        //List<GameEvent.Response> list = new ArrayList<>(listenerList.size());
        List<Callable<GameEvent.Response>> tasksTodo = new ArrayList<>(listenerList.size());
        for (var listenerEntry : listenerMap.entrySet()) {
            tasksTodo.add(() -> callListener(listenerEntry.getKey(), listenerEntry.getValue(), gameEvent));
        }
        try {
            return threadPool.invokeAll(tasksTodo).stream().map(responseFuture -> {
                try {
                    return responseFuture.get();
                } catch (InterruptedException | ExecutionException e) {
                    return new GameEvent.ClientErrorResponse(e);
                }
            }).collect(Collectors.toList());
        } catch (InterruptedException e) {
            throw new IllegalStateException("Unable to deal with interrupted exception");
        }
    }

    /**
     * Publish game event to exact node and get response
     *
     * @param nodeIdentity
     * @param gameEvent
     * @return
     */
    public GameEvent.Response publish(NodeIdentity nodeIdentity, GameEvent gameEvent) {
        var listener = listenerMap.getOrDefault(nodeIdentity.id, null);
        if (listener == null) {
            return null;
        }
        return callListener(nodeIdentity.id, listener, gameEvent);
    }

    public GameEvent.Response publish(UUID uuid, GameEvent gameEvent) {
        var nodeId = NodeIdentityManager.getIdentityById(uuid);
        return this.publish(nodeId, gameEvent).withRespondent(nodeId.id);
    }

}
