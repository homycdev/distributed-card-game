package dsmpf.holdem.network;

import java.rmi.RemoteException;
import java.util.*;
import java.util.logging.Logger;

public class NodeIdentityManager {

    private static final Logger logger = Logger.getLogger("NodeIdentityManager");
    private static final UUID selfUUID = UUID.randomUUID();
    private static NodeIdentity selfIdentity = new NodeIdentity(selfUUID, 0);
    public static Map<UUID, NodeIdentity> identityMap = new HashMap<>();
    public static List<NodeIdentity> identityList = new ArrayList<>();
    public static RegistrationRmiService.OnNodeChangeListener onNodeChangeListener;
    public static List<RegistrationRmiService.OnNodeChangeListener> onNodeChangeListenerList = new ArrayList<>();
    private static boolean dummySelfIdentity = true;

    /**
     * Registration of local listeners on events of node list update
     *
     * @param listener
     */
    public static void addOnNodeChangeListenerList(RegistrationRmiService.OnNodeChangeListener listener) {
        onNodeChangeListenerList.add(listener);
    }

    /**
     * Deletion of local listeners on events of node list update
     *
     * @param listener
     */
    public static void removeOnNodeChangeListenerList(RegistrationRmiService.OnNodeChangeListener listener) {
        onNodeChangeListenerList.remove(listener);
    }

    /**
     * Get identity of current node
     * Returns "dummy" identity, if node have not been registered
     *
     * @return identity of current node
     */
    public static NodeIdentity getSelfIdentity() {
        return selfIdentity;
    }

    /**
     * If we are starting the cluster from zero, node registers itself and waits for others to connect
     *
     * @param advertisedAddress
     * @param rmiPort
     * @param isBehindNat
     * @return
     */
    public static NodeIdentity registerSelf(String advertisedAddress, int rmiPort, boolean isBehindNat) {
        if (selfIdentity != null && !dummySelfIdentity) {
            logger.warning("repeated node registration is not allowed, ignoring");
            return selfIdentity;
        }
        selfIdentity = register(new NodeIdentity(
                NodeIdentityManager.selfUUID,
                advertisedAddress,
                isBehindNat,
                rmiPort
        ));
        dummySelfIdentity = false;
        logger.info("Current node is registered : " + selfIdentity.toString());
        return selfIdentity;
    }

    /**
     * If we have some bootstrap node we are using registration service for remote service so we can be reigtered
     * and notify all other nodes
     *
     * @param bootstrapService
     * @param advertisedAddress
     * @param rmiPort
     * @param isBehindNat
     * @return
     * @throws RemoteException
     */
    public static NodeIdentity registerSelf(RegistrationRmiService bootstrapService, String advertisedAddress, int rmiPort, boolean isBehindNat) throws RemoteException {
        if (selfIdentity != null && !dummySelfIdentity) {
            logger.warning("repeated node registration is not allowed, ignoring");
            return selfIdentity;
        }
        if (bootstrapService == null) {
            logger.severe("register with bootstrap service, but it is null");
            return null;
        }
        selfIdentity = bootstrapService.registerNewNode(new NodeIdentity(
                NodeIdentityManager.selfUUID,
                advertisedAddress,
                isBehindNat,
                rmiPort
        ));
        logger.info("Current node is registered: " + selfIdentity.toString());
        dummySelfIdentity = false;
        if (isBehindNat) {
            onNodeChangeListener = new RegistrationRmiService.OnNodeChangeListener() {
                @Override
                public void onNodeRegister(NodeIdentity newRegisteredNode) {
                    logger.info("Added new node " + newRegisteredNode.id + "to node list via callback");
                    NodeIdentityManager.register(newRegisteredNode);
                }

                @Override
                public void onNodeUnregister(NodeIdentity removedNode) {
                    logger.info("Removed node " + removedNode.id + "from node list via callback");
                    NodeIdentityManager.unregister(removedNode);
                }
            };
            bootstrapService.addOnNodeChangeListener(selfIdentity, onNodeChangeListener);
            logger.info("Current node have subscribed to node list changes");
        } else {
            logger.info("Current node is not behind nat and will receive node list changes via pushes");
        }
        var knownNodes = bootstrapService.getKnownNodesList();
        knownNodes.forEach(NodeIdentityManager::register);
        logger.info("Fetched " + knownNodes.size() + " known nodes from bootstrap server");
        return selfIdentity;
    }

    /**
     * Register of node identity.
     *
     * @param identity
     * @return
     */
    public static NodeIdentity register(NodeIdentity identity) {
        if (getNodesCount() == 0) {
            // TODO: make dealer elections
            // set the first node as dealer
            identity.isDealer = true;
        }
        identityMap.put(identity.id, identity);
        identityList.add(identity);

        onNodeChangeListenerList.forEach(listener -> {
            try {
                listener.onNodeRegister(identity);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });

        return identity;
    }

    /**
     * Delete node by identity
     *
     * @param identity
     */
    public static void unregister(NodeIdentity identity) {
        identityMap.remove(identity.id);
        identityList.remove(identity);

        onNodeChangeListenerList.forEach(listener -> {
            try {
                listener.onNodeUnregister(identity);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });
    }

    public static int getNodesCount() {
        if (identityMap.size() != identityList.size()) {
            logger.severe("identityMap.size != identityList.size, program logic error");
        }
        return identityList.size();
    }

    public static NodeIdentity getIdentityById(UUID nodeId) {
        return identityMap.getOrDefault(nodeId, null);
    }

    public static List<NodeIdentity> getIdentityList() {
        return Collections.unmodifiableList(identityList);
    }

    public static Map<UUID, NodeIdentity> getIdentityMap() {
        return Collections.unmodifiableMap(identityMap);
    }
}
