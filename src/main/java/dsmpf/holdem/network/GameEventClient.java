package dsmpf.holdem.network;

import dsmpf.holdem.core.GameEvent;

import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.UUID;

public class GameEventClient {

    public static interface Listener extends Remote, Serializable {
        public GameEvent.Response onGameEvent(GameEvent event) throws RemoteException;
    }

    private final RmiServiceDiscovery rmiServiceDiscovery;
    private final Listener internalListener;
    private final Listener clientSideListener;
    private UUID currentDealerNode;
    private Scanner scanner;
    private final Object waitObject = new Object();

    public GameEventClient(RmiServiceDiscovery rmiServiceDiscovery, UUID startupDealerNode) throws RemoteException {
        this.rmiServiceDiscovery = rmiServiceDiscovery;
        this.currentDealerNode = startupDealerNode;
        this.scanner = new Scanner(System.in);
        this.clientSideListener = new Listener() {
            @Override
            public GameEvent.Response onGameEvent(GameEvent event) throws RemoteException {
                switch (event.command) {
                    case EXIT:
                        synchronized (waitObject) {
                            waitObject.notify();
                        }
                        return new GameEvent.ActionResponse("ok");
                    case INFO:
                        System.out.println(event.message);
                        return new GameEvent.InfoResponse("ok");
                    case ACTION:
                        System.out.println(event.message);
                        System.out.print("your action: ");
                        System.out.flush();
                        //TODO: read actions;
                        return new GameEvent.ActionResponse(scanner.nextLine());
                }
                return new GameEvent.ClientErrorResponse(new IllegalStateException("No response provided"));
            }
        };
        this.internalListener = new ListenerImpl(clientSideListener);
    }

    private static class ListenerImpl extends UnicastRemoteObject implements Listener {
        Listener clientSideListener;

        public ListenerImpl(Listener clientSideListener) throws RemoteException {
            super();
            this.clientSideListener = clientSideListener;
        }

        @Override
        public GameEvent.Response onGameEvent(GameEvent event) throws RemoteException {
            return this.clientSideListener.onGameEvent(event);
        }
    }

    public void loop(Listener listener) throws RemoteException, NotBoundException {
        var publisher = (GameEventServer.Publisher) rmiServiceDiscovery.lookup(currentDealerNode, GameEventServer.Publisher.NAME);
        publisher.addSubscriberListener(NodeIdentityManager.getSelfIdentity(), internalListener);
        synchronized (waitObject) {
            try {
                waitObject.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
