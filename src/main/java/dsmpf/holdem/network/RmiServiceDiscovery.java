package dsmpf.holdem.network;

import java.net.InetSocketAddress;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RmiServiceDiscovery {

    private Registry localRegistry;
    private NodeIdentity selfIdentity;
    private Map<UUID, Registry> directRemoteRegistryMap;

    private HolePunchingRmiService availableHolePunchingService;
    private HolePunchingRmiSocketFactory holePunchingSocketFactory;

    /**
     * Constructor of abstract service discoverer
     *
     * @param registry local registry to store rmi services
     */
    public RmiServiceDiscovery(Registry registry) {
        localRegistry = registry;
        selfIdentity = NodeIdentityManager.getSelfIdentity();
        directRemoteRegistryMap = new HashMap<>();
        holePunchingSocketFactory = new HolePunchingRmiSocketFactory(selfIdentity);
    }

    /**
     * This method enables access to service hidden behind the nat via usage of TCP Hole Puncher
     *
     * @param holePuncherEndpoint hole puncher public endpoint
     */
    public void setTcpHolePuncherEndpoint(InetSocketAddress holePuncherEndpoint) {
        holePunchingSocketFactory.setMediatorSocketAddress(holePuncherEndpoint);
    }

    /**
     * This method gets serviceName and serviceImpl and wraps it to acceptable RMI format.
     *
     * @param serviceName
     * @param serviceImpl
     * @param <S>
     * @return
     * @throws RemoteException
     * @throws AlreadyBoundException
     */
    public <S extends Remote> S register(String serviceName, S serviceImpl) throws RemoteException, AlreadyBoundException {
        localRegistry.bind(serviceName, UnicastRemoteObject.exportObject(serviceImpl,
                0));
        return serviceImpl;
    }

    /**
     * Deletes service by its name from RMI registry
     *
     * @param serviceName
     * @param <S>
     * @throws RemoteException
     * @throws NotBoundException
     */
    public <S extends Remote> void unregister(String serviceName) throws RemoteException, NotBoundException {
        localRegistry.unbind(serviceName);
    }

    /**
     * Direct look up of registry by address of remote node in RMI registry
     *
     * @param address
     * @param serviceName
     * @param <S>
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public <S extends Remote> S lookup(InetSocketAddress address, String serviceName) throws RemoteException, NotBoundException {
        var remoteRegistry = LocateRegistry.getRegistry(address.getHostName(), address.getPort());
        //noinspection unchecked
        return (S) remoteRegistry.lookup(serviceName);
    }

    /**
     * Universal lookup which will try to find the service
     *
     * @param nodeId
     * @param serviceName
     * @param <S>
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public <S extends Remote> S lookup(UUID nodeId, String serviceName) throws RemoteException, NotBoundException {
        if (nodeId == null) {
            throw new RemoteException("Node id should not be null");
        }
        if (NodeIdentityManager.getSelfIdentity() == null) {
            throw new RemoteException("Node should be registered via NodeIdentityManager");
        }
        if (NodeIdentityManager.getSelfIdentity().id.equals(nodeId)) {
            //noinspection unchecked
            return (S) localRegistry.lookup(serviceName);
        }
        var nodeIdentity = NodeIdentityManager.getIdentityById(nodeId);
        if (nodeIdentity == null) {
            throw new RemoteException("Node with id = " + nodeId.toString() + " is not registered");
        }

        if (nodeIdentity.isBehindNat) {
            if (availableHolePunchingService == null) {
                var holePunchingServiceIdentity = NodeIdentityManager.getIdentityList().stream()
                        .filter(remoteIdentity -> !remoteIdentity.isBehindNat)
                        .filter(remoteIdentity -> remoteIdentity.publicAddress != null)
                        .findAny()
                        .orElseThrow(() -> new RemoteException("No hole punching service is available and the target service is behind nat"));
                // lookup the service
                availableHolePunchingService = lookup(holePunchingServiceIdentity.id, HolePunchingRmiService.NAME);
                setTcpHolePuncherEndpoint(new InetSocketAddress(
                        holePunchingServiceIdentity.publicAddress,
                        availableHolePunchingService.getHolePuncherPort()
                ));
            }
        }

        if (directRemoteRegistryMap.containsKey(nodeId)) {
            //noinspection unchecked
            return (S) directRemoteRegistryMap.get(nodeId).lookup(serviceName);
        } else {
            var remoteRegistry = LocateRegistry.getRegistry(nodeIdentity.publicAddress, nodeIdentity.rmiPort);
            directRemoteRegistryMap.put(nodeId, remoteRegistry);
            //noinspection unchecked
            return (S) remoteRegistry.lookup(serviceName);
        }
    }
}
