package dsmpf.holdem.network;

import java.io.Serializable;
import java.util.UUID;

public class NodeIdentity implements Serializable {

    // unique id of node
    public UUID id;

    // address
    public String publicAddress;

    // local address
    public String localAddress;

    // true if client is behind nat and could use some help in connections
    public boolean isBehindNat;

    // port of rmi services
    public int rmiPort;

    // true if this node is current dealer
    public boolean isDealer = false;

    public NodeIdentity(UUID id, int rmiPort) {
        this.id = id;
        this.rmiPort = rmiPort;
    }

    public NodeIdentity(UUID id, String address, boolean isBehindNat, int rmiPort) {
        this.id = id;
        if (!isBehindNat) {
            this.publicAddress = address;
        } // else if would be filled by registrar
        this.localAddress = address;
        this.isBehindNat = isBehindNat;
        this.rmiPort = rmiPort;
    }

    @Override
    public String toString() {
        return "NodeIdentity{" +
                "id=" + id +
                ", publicAddress='" + publicAddress + '\'' +
                ", isBehindNat=" + isBehindNat +
                ", rmiPort=" + rmiPort +
                ", isDealer=" + isDealer +
                '}';
    }
}
