package dsmpf.holdem.utils;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ProgramArgumentParser {

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface Value {
        String name() default "";

        int argCount() default 1;
    }

    public ProgramArgumentParser() {
    }

    public ProgramArgumentDescriptor descriptorOfField(Object target, Field field) {
        var valueData = field.getAnnotation(Value.class);
        var consumer = getValueConsumerByClass(target, field);
        return new ProgramArgumentDescriptor(
                valueData.argCount(),
                valueData.name(),
                valueData.name(),
                consumer
        );
    }

    public <T> T parse(String[] args, T configValue) throws IOException {
        parse(args, Arrays.stream(configValue.getClass().getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Value.class))
                .peek(AccessibleObject::trySetAccessible)
                .map(field -> descriptorOfField(configValue, field))
                .collect(Collectors.toList()));
        return configValue;
    }

    public void parse(String[] args, List<ProgramArgumentDescriptor> callbacks) throws IOException {
        Map<String, ProgramArgumentDescriptor> longArgsHandlers = new HashMap<>();
        Map<String, ProgramArgumentDescriptor> shortArgsHandlers = new HashMap<>();
        callbacks.forEach(descriptor -> {
            longArgsHandlers.put(descriptor.getLongName(), descriptor);
            shortArgsHandlers.put(descriptor.getShortName(), descriptor);
        });
        for (int i = 0; i < args.length; i++) {
            if (!args[i].startsWith("-")) {
                throw new IOException("program flag expected");
            }
            var isLongFlag = args[i].startsWith("--");
            var flagName = args[i].substring(isLongFlag ? 2 : 1);
            ProgramArgumentDescriptor argumentDescriptor = isLongFlag ?
                    Optional.ofNullable(longArgsHandlers.getOrDefault(flagName, null))
                            .orElseThrow(() -> new IOException("no callback is found for long flag " + flagName)) :
                    Optional.ofNullable(shortArgsHandlers.getOrDefault(flagName, null))
                            .orElseThrow(() -> new IOException("no callback is found for short flag " + flagName));
            List<String> argsList = new ArrayList<>(argumentDescriptor.getArgCount());
            for (i++; i < args.length && argsList.size() < argumentDescriptor.getArgCount(); i++) {
                argsList.add(args[i]);
            }
            i -= 1;
            argumentDescriptor.getCallback().accept(argsList);
        }
    }

    public Consumer<List<String>> getValueConsumerByClass(Object object, Field field) {
        var fieldType = field.getType();
        if (fieldType == String.class) {
            return (list) -> {
                try {
                    field.set(object, list.get(0));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            };
        }
        if (fieldType == int.class) {
            return (list) -> {
                try {
                    var fieldValue = Integer.parseInt(list.get(0));
                    field.set(object, fieldValue);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            };
        }
        if (fieldType == boolean.class) {
            return (list) -> {
                var fieldValue = list.isEmpty() || list.get(0).equals("true");
                try {
                    field.set(object, fieldValue);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            };
        }
        throw new UnsupportedOperationException("value consumer is not implemented for field type \"" + fieldType.getName() + "\"");
    }
}
