package dsmpf.holdem.utils

import java.util.function.Consumer

class ProgramArgumentDescriptor(
    val argCount: Int,
    val longName: String,
    val shortName: String,
    val callback: Consumer<List<String>>
)