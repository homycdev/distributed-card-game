package dsmpf.holdem.utils;

import dsmpf.holdem.core.GameEvent;
import dsmpf.holdem.network.GameEventClient;
import dsmpf.holdem.network.RmiServiceDiscovery;
import dsmpf.holdem.storage.KeyValueStorage;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Objects;
import java.util.UUID;

public class GameShell {

    private final RmiServiceDiscovery rmiServiceDiscovery;
    private final KeyValueStorage keyValueStorage;
    private GameEventClient gameEventClient;

    public GameShell(RmiServiceDiscovery rmiServiceDiscovery, KeyValueStorage keyValueStorage) {
        this.rmiServiceDiscovery = rmiServiceDiscovery;
        this.keyValueStorage = keyValueStorage;
    }

    public int loop() {
        var dealerUuid = (UUID) Objects.requireNonNull(this.keyValueStorage.get("game:dealer_id"));
        try {
            this.gameEventClient = new GameEventClient(rmiServiceDiscovery, dealerUuid);
            //TODO: receive custom events
            gameEventClient.loop(new GameEventClient.Listener() {
                @Override
                public GameEvent.Response onGameEvent(GameEvent event) throws RemoteException {
                    return null;
                }
            });
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
            return 1;
        }
        return 0;
    }
}
