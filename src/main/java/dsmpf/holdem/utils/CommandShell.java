package dsmpf.holdem.utils;

import dsmpf.holdem.Main;
import dsmpf.holdem.network.NodeIdentityManager;
import dsmpf.holdem.network.PingRmiService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

public class CommandShell {

    public int loop() {
        var scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            var nextLine = scanner.nextLine();
            switch (nextLine) {
                case "pingall": {
                    var identityList = NodeIdentityManager.getIdentityList();
                    System.out.println("Known nodes = " + identityList.size());
                    identityList.forEach(identity -> {
                        try {
                            var pingService = (PingRmiService) Main.rmiServiceDiscovery.lookup(identity.id, PingRmiService.NAME);
                            System.out.println(identity.id + " answers: " + pingService.ping());
                        } catch (RemoteException | NotBoundException e) {
                            System.out.println(identity.id + " exception: " + e.getMessage());
                            e.printStackTrace();
                        }
                    });
                    continue;
                }
                case "exit": {
                    return 0;
                }
            }
        }
        return 1;
    }
}
